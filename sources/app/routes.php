<?

//ACCUEIL
$app->get('/', function () use ($app) {
    $app->redirect($app->urlFor('accueil'));
})->name('root');

$app->get('/accueil', function () use ($app) {
    $c = new Controller($app);
    $c->accueil();
})->name('accueil');


$app->get('/detailsAnnonce:id', function ($id) use ($app) {
    $c = new Controller($app);
    $c->header();
    $c->detailsAnnonce($id);
    $c->footer();
})->name('detAnnonce');


//RECHERCHE
$app->get('/moteurRecherche', function () use ($app) {
    $c = new Controller($app);
    $c->moteurRecherche();
})->name('rechAnnonce');

$app->get('/resultatRecherche', function () use ($app) {
    $c = new Controller($app);
    $c->search();
})->name('resRecherche');


//GESTION ANNONCE
$app->get('/saisirAnnonce', function () use ($app) {
    $c = new Controller($app);
    $c->saisieAjoutAnnonce();
})->name('ajAnnonce');

$app->post('/insererAnnonce', function () use ($app) {
    $c = new Controller($app);
    $c->insertAnnonce();
})->name('insAnnonce');

$app->get('/supprimerAnnonce:id', function ($id) use ($app) {
    $c = new Controller($app);
    $c->supprimerAnnonce($id);
})->name('supprAnnonce');

$app->post('/suppressionAnnonce:id', function ($id) use ($app) {
    $c = new Controller($app);
    $c->supprAnnonce($id);
})->name('supAnnonce');

$app->get('/gestionAnnonce', function () use ($app) {
    $c = new Controller($app);
    $c->gestionAnnonce();
})->name('gestAnnonce');

$app->get('/gererAnnonces', function () use ($app) {
    $c = new Controller($app);
    $c->gererAnnonce();
})->name('gerAnnonce');

$app->get('/contactVendeur:id', function ($id) use ($app) {
    $c = new Controller($app);
    $c->contacterVendeur($id);
})->name('contVendeur');


?>