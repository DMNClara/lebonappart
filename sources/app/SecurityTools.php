<?php

class SecurityTools
{
    //pour les MPD : on couple avec un grain de sel et on crypte le tout
    public static function SaltSha1Crypt($mdpClair)
    {
        return sha1(sha1($mdpClair) . "$2y$07");
    }

    //si les valeurs entrée sont négatives on remet à 0
    public static function forcerPositive($in)
    {
        if ($in < 0) {
            return 0;
        } else return $in;
    }
}

