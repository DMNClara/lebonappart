<?php

class Controller
{

    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function header()
    {
        $this->app->render('header.twig');
    }

    public function listedannonces($annonces)
    {
        $this->app->render('afficherlistedannonces.twig', array('annonces' => $annonces));
    }

    public function afficheErreur($erreur)
    {
        $this->app->render('info_erreur.twig', array('erreur' => $erreur));
    }

    public function footer()
    {
        $this->app->render('footer.twig');
    }

    public function accueil()
    {
        $this->header();
        $this->moteurRecherche();
        $annonces = Annonce::getAllAnnonces();
        $this->listedannonces($annonces);
    }

    public function detailsAnnonce($id)
    {
        $annonce = Annonce::getDetailsAnnonce($id);
        $photos =Photo::getDetailsPhotos($id);
        $this->app->render('details_annonce.twig', array('annonce' => $annonce, 'photos' => $photos));
    }

    public function moteurRecherche()
    {
        $villes = Ville::All();
        $typeBiens = TypeBien::All();
        $typeTransactions = TypeTransaction::All();
        $this->app->render('saisie_rech_annonce.twig', array(
            'villes' => $villes,
            'typeBiens' => $typeBiens,
            'typeTransactions' => $typeTransactions
        ));
    }

    public function search()
    {
        //lecture des champs saisis
        $keyword = filter_var($this->app->request->get('keyword'), FILTER_SANITIZE_STRING);
        $priceIn = filter_var($this->app->request->get('priceIn'), FILTER_SANITIZE_NUMBER_INT);
        $priceOut = filter_var($this->app->request->get('priceOut'), FILTER_SANITIZE_NUMBER_INT);
        $superficie = filter_var($this->app->request->get('superficie'), FILTER_SANITIZE_NUMBER_INT);
        $superficieMarge = filter_var($this->app->request->get('superficieMarge'), FILTER_SANITIZE_NUMBER_INT);
        $nbPieces = filter_var($this->app->request->get('nbpieces'), FILTER_SANITIZE_NUMBER_INT);
        $nbPiecesMarge = filter_var($this->app->request->get('nbPiecesMarge'), FILTER_SANITIZE_NUMBER_INT);
        $idville = filter_var($this->app->request->get('ville'), FILTER_SANITIZE_NUMBER_INT);
        $typeBien = filter_var($this->app->request->get('typeBien'), FILTER_SANITIZE_NUMBER_INT);
        $typeTransaction = filter_var($this->app->request->get('id_type_transaction'), FILTER_SANITIZE_NUMBER_INT);

        //si les valeurs entrée sont négatives on remet à 0
        $priceIn = SecurityTools::forcerPositive($priceIn);
        $priceOut = SecurityTools::forcerPositive($priceOut);
        $superficie = SecurityTools::forcerPositive($superficie);
        $superficieMarge = SecurityTools::forcerPositive($superficieMarge);
        $nbPieces = SecurityTools::forcerPositive($nbPieces);
        $nbPiecesMarge = SecurityTools::forcerPositive($nbPiecesMarge);

        //création de la requete
        $query = Annonce::orderBy('created_at', 'DESC')
            ->with('quartier', 'transaction', 'bien', 'quartier.ville');

        $i = 0;

        //soit on parcours un tableau qui complete automatiquement la requete, soit j'ajoute les where a la main
        // selon les cas de figure
        if ((strlen($keyword) > 0)) {
            if ($this->app->request->get('title_only')) {
                $choix[$i++] = "titre";
                $choix[$i++] = "like";
                $choix[$i++] = "%" . $keyword . "%";
            } else {
                $query->where('titre', 'like', '%' . $keyword . '%');
                $query->orWhere('texte', 'like', '%' . $keyword . '%');
            }
        }

        if ($priceIn < $priceOut && $priceOut > 0) {
            $choix[$i++] = "prix_bien";
            $choix[$i++] = ">=";
            $choix[$i++] = $priceIn;
            $choix[$i++] = "prix_bien";
            $choix[$i++] = "<=";
            $choix[$i++] = $priceOut;
        }

        if ($superficie > 0) {
            $choix[$i++] = "superficie";
            $choix[$i++] = ">=";
            $choix[$i++] = ($superficie - $superficieMarge);
            $choix[$i++] = "superficie";
            $choix[$i++] = "<=";
            $choix[$i++] = ($superficie + $superficieMarge);
        }

        if ($nbPieces > 0) {
            $choix[$i++] = "nb_piece";
            $choix[$i++] = ">=";
            $choix[$i++] = ($nbPieces - $nbPiecesMarge);
            $choix[$i++] = "nb_piece";
            $choix[$i++] = "<=";
            $choix[$i++] = ($nbPieces + $nbPiecesMarge);
        }

        if ($idville > 0) {
            $query->whereIn("id_quartier", Ville::villeOuEstQuartier($idville));
        }

        if ($typeBien > 0) {
            $choix[$i++] = "id_type_bien";
            $choix[$i++] = "=";
            $choix[$i++] = $typeBien;
        }

        if ($typeTransaction > 0) {
            $choix[$i++] = "id_type_transaction";
            $choix[$i++] = "=";
            $choix[$i++] = $typeTransaction;
        }

        //on ajoute toutes les sous requetes stockées dans le tableau a la requete
        for ($k = 0; $k < $i; $k += 3) {
            $query->where($choix[$k], $choix[$k + 1], $choix[$k + 2]);
        }

        //execution de la requete
        $annonces = $query->get();

        //rendu
        $this->header();
        $this->moteurRecherche();
        $this->app->render('info_resultat_recherche.twig');
        if (count($annonces) > 0) {
            $this->listedannonces($annonces);
        } else {
            $this->app->render('info_erreur.twig', array('erreur' => "Aucune annonce trouvée."));
        }
        $this->footer();
    }


    public
    function saisieAjoutAnnonce()
    {
        $this->header();
//      $villes = Ville::All();
        $typeBiens = TypeBien::All();
        $typeTransactions = TypeTransaction::All();
        $quartiers = Quartier::All();
        $this->app->render('saisie_ajout_annonce.twig', array(
//            'villes' => $villes,
            'quartiers' => $quartiers,
            'typeBiens' => $typeBiens,
            'typeTransactions' => $typeTransactions
        ));
        $this->footer();
    }

    public
    function insertAnnonce()
    {
        //lecture des champs saisis & filtrage des caracteres spéciaux
        $nom_vendeur = filter_var($this->app->request->post('nom_vendeur'), FILTER_SANITIZE_STRING);
        $tel_vendeur = filter_var($this->app->request->post('tel_vendeur'), FILTER_SANITIZE_NUMBER_INT);
        $mail_vendeur = filter_var($this->app->request->post('mail_vendeur'), FILTER_SANITIZE_EMAIL);
        $mdp_annonce = filter_var($this->app->request->post('mdp_annonce'), FILTER_SANITIZE_STRING);
        $mdp_annonce_2 = filter_var($this->app->request->post('mdp_annonce_2'), FILTER_SANITIZE_STRING);
        $titre = filter_var($this->app->request->post('titre'), FILTER_SANITIZE_STRING);
        $texte = filter_var($this->app->request->post('texte'), FILTER_SANITIZE_STRING);
        $prix_bien = filter_var($this->app->request->post('prix'), FILTER_SANITIZE_NUMBER_INT);
        $superficie = filter_var($this->app->request->post('superficie'), FILTER_SANITIZE_NUMBER_INT);
        $nb_piece = filter_var($this->app->request->post('nb_piece'), FILTER_SANITIZE_NUMBER_INT);
        $id_quartier = filter_var($this->app->request->post('id_quartier'), FILTER_SANITIZE_NUMBER_INT);
        $id_type_transaction = filter_var($this->app->request->post('id_type_transaction'), FILTER_SANITIZE_NUMBER_INT);
        $id_type_bien = filter_var($this->app->request->post('id_type_bien'), FILTER_SANITIZE_NUMBER_INT);

        //vérification annonce
        $creation_ok = Annonce::verifierAnnonce($nom_vendeur, $tel_vendeur, $mail_vendeur, $mdp_annonce,
            $mdp_annonce_2, $titre, $texte, $prix_bien, $superficie, $nb_piece, $id_quartier,
            $id_type_transaction, $id_type_bien);

        switch ($creation_ok) {
            case 0:
                $id = Annonce::ajouterAnnonce($nom_vendeur, $tel_vendeur, $mail_vendeur, $mdp_annonce,
                    $titre, $texte, $prix_bien, $superficie, $nb_piece, $id_quartier,
                    $id_type_transaction, $id_type_bien);
                $this->header();
                $this->app->render('confirm_ajout_annonce.twig');
                $this->detailsAnnonce($id);
                $this->footer();
                break;
            case 1:
                $erreur = "Il manque une information, ou l'une d'elles est incorrecte.";
                $this->header();
                $this->afficheErreur($erreur);
                break;
            case 2:
                $erreur = "N° de tel incorrect";
                $this->header();
                $this->afficheErreur($erreur);
                break;
            case 3:
                $erreur = "Addresse email incorrecte";
                $this->header();
                $this->afficheErreur($erreur);
                break;
            case 4:
                $erreur = "Erreur entre les deux mots de passe";
                $this->header();
                $this->afficheErreur($erreur);
                break;
            case 5:
                $erreur = "Mot de passe trop court";
                $this->header();
                $this->afficheErreur($erreur);
                break;
            case 6:
                $erreur = "Le prix n'est pas valide";
                $this->header();
                $this->afficheErreur($erreur);
                break;
        }
    }

    public
    function gestionAnnonce()
    {
        $this->header();
        $this->app->render('saisie_gestion_annonce.twig');
        $this->footer();
    }

    public
    function gererAnnonce()
    {
        //lecture du mail
        $mail_vendeur = filter_var($this->app->request->get('mail_annonce'), FILTER_SANITIZE_EMAIL);
        $annonces = Annonce::getByMail($mail_vendeur);

        $this->header();
        if (count($annonces) > 0) {
            $this->app->render('info_annoncestrouveesparmail.twig');
            $this->listedannonces($annonces);
        } else {
            $this->app->render('info_erreur.twig', array('erreur' => "Aucune annonce trouvée."));
        }
        $this->footer();
    }


    public
    function supprAnnonce($id)
    {
        $this->header();
        $mdp_vendeur = filter_var($this->app->request->post('mdp_vendeur'), FILTER_SANITIZE_STRING);
        $bool = Annonce::checkPassword($id, $mdp_vendeur);

        if ($bool == true) {
            Annonce::delAnnonce($id);
            $this->app->render('confirm_suppr_annonce.twig');
        } else {
            $erreur = 'Mot de passe incorrect';
            $this->afficheErreur($erreur);
        }
        $this->footer();
    }

    public
    function supprimerAnnonce($id)
    {
        $this->header();
        $this->app->render('saisie_supprimer_annonce.twig', array('id' => $id));
        $this->footer();
    }

    public
    function contacterVendeur($id)
    {
        $this->header();
        $annonce = Annonce::find($id);
        $this->app->render('details_vendeur.twig', array('annonce' => $annonce));
        $this->footer();
    }

}


