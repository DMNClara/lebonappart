<?php
/**
 * Created by PhpStorm.
 * User: clarademoliner
 * Date: 21/10/15
 * Time: 18:19
 */

use Illuminate\Database\Eloquent\Model as Eloquent;

class TypeBien extends Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'TypeBiens';

    public function annonces(){
        return $this->hasMany('Annonce', 'id_type_bien');
    }

}
?>