<?php
/**
 * Created by PhpStorm.
 * User: clarademoliner
 * Date: 21/10/15
 * Time: 18:16
 */

use Illuminate\Database\Eloquent\Model as Eloquent;

class Photo extends Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Photos';


    public function annonce()
    {
        return $this->belongsTo('Annonce', 'id_annonce');
    }

    public static function getIDAnnonce($id)
    {
        $photo = Photo::find($id);

        if ($photo == NULL) {
            return 0;
        } else {
            $idPhoto = $photo->id_annonce;
            return $idPhoto;
        }
    }

    public static function getDetailsPhotos($id)
    {
        $idAnnonce = Photo::getIDAnnonce($id);

        if ($idAnnonce == 0) {

        }
        else {
            return Photo::where('id_annonce', '=', $idAnnonce )->get();
        }
    }
}

?>