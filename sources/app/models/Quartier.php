<?php
/**
 * Created by PhpStorm.
 * User: clarademoliner
 * Date: 21/10/15
 * Time: 18:18
 */

use Illuminate\Database\Eloquent\Model as Eloquent;

class Quartier extends Eloquent
{
    protected $table = 'Quartiers';

    public function ville() {
        return $this->belongsTo('Ville', 'id_ville');
    }

    public function annonces(){
        return $this->hasMany('Annonce', 'id_quartier');
    }

}
?>