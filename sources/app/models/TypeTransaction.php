<?php
/**
 * Created by PhpStorm.
 * User: clarademoliner
 * Date: 21/10/15
 * Time: 18:20
 */
use Illuminate\Database\Eloquent\Model as Eloquent;

class TypeTransaction extends Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'TypeTransactions';

    public function annonces(){
        return $this->hasMany('Annonce', 'id_type_transaction');
    }
}
?>