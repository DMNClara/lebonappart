<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Annonce extends Eloquent
{
    protected $table = 'Annonces';
    protected $hidden = array('mdp_annonce');

    public function quartier()
    {
        return $this->belongsTo('Quartier', 'id_quartier');
    }

    public function transaction()
    {
        return $this->belongsTo('TypeTransaction', 'id_type_transaction');
    }

    public function bien()
    {
        return $this->belongsTo('TypeBien', 'id_type_bien');
    }

    public function photos()
    {
        return $this->hasMany('Annonce', 'id_annonce');
    }

    public static function getAllAnnonces()
    {
        return Annonce::orderBy('created_at', 'DESC')
            ->with('quartier', 'transaction', 'bien', 'quartier.ville')
            ->get();
    }

    public static function getDetailsAnnonce($id)
    {
        return Annonce::with('quartier', 'transaction', 'bien', 'quartier.ville')->find($id);
    }

    public static function verifierAnnonce($nom_vendeur, $tel_vendeur, $mail_vendeur, $mdp_annonce,
                                           $mdp_annonce_2, $titre, $texte, $prix_bien, $superficie, $nb_piece,
                                           $id_quartier, $id_type_transaction, $id_type_bien)
    {
        //verif texte saisi
        if ($nom_vendeur == ""
            || $titre == ""
            || $texte == ""
            || $superficie <= 0
            || $nb_piece <= 0
            || $id_quartier <= 0
            || $id_type_transaction <= 0
            || $id_type_bien <= 0
        ) {
            return 1;
        }
        //verif telephone valide
        if (strlen($tel_vendeur) != 10 || !(ctype_digit($tel_vendeur))) {
            return 2;
        }
        //verif email valide
        if (!filter_var($mail_vendeur, FILTER_VALIDATE_EMAIL)) {
            return 3;
        }
        // verifier si confirmation pass ok
        if (!($mdp_annonce === $mdp_annonce_2)) {
            return 4;
        }
        //verif longueur mdp
        if (strlen($mdp_annonce) < 5) {
            return 5;
        }
        //verif prix bien
        if (!empty($prix_bien) && (!ctype_digit($prix_bien))) {
            return 6;
        } else {
            return 0;
        }
    }

    public static function ajouterAnnonce($nom_vendeur, $tel_vendeur, $mail_vendeur, $mdp_annonce, $titre, $texte, $prix_bien, $superficie, $nb_piece,
                                          $id_quartier, $id_type_transaction, $id_type_bien)
    {
        try {
            $annonce = new Annonce;
            $annonce->nom_vendeur = $nom_vendeur;
            $annonce->tel_vendeur = $tel_vendeur;
            $annonce->mail_vendeur = $mail_vendeur;
            $annonce->mdp_annonce = SecurityTools::SaltSha1Crypt($mdp_annonce);
            $annonce->titre = $titre;
            $annonce->texte = $texte;
            $annonce->prix_bien = $prix_bien;
            $annonce->superficie = $superficie;
            $annonce->nb_piece = $nb_piece;
            $annonce->id_quartier = $id_quartier;
            $annonce->id_type_transaction = $id_type_transaction;
            $annonce->id_type_bien = $id_type_bien;
            $annonce->save();
            return $annonce->id;
        } catch (Exception $e) {
        }
    }

    public static function getByMail($mail_vendeur)
    {
        return Annonce::orderBy('created_at', 'DESC')
            ->with('quartier', 'transaction', 'bien', 'quartier.ville')
            ->where('mail_vendeur', '=', $mail_vendeur)
            ->get();
    }

    public static function checkPassword($id, $mdp_vendeur)
    {
        $annonce = Annonce::find($id);
        $mdp = $annonce->mdp_annonce;
        return (SecurityTools::SaltSha1Crypt($mdp_vendeur) == $mdp);
    }

    public static function delAnnonce($id)
    {
        try {
            $annonce = Annonce::find($id);
            $annonce->delete();
        } catch (Exception $e) {
        }
    }
}