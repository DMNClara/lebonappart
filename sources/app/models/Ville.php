<?php
/**
 * Created by PhpStorm.
 * User: clarademoliner
 * Date: 21/10/15
 * Time: 18:21
 */

use Illuminate\Database\Eloquent\Model as Eloquent;

class Ville extends Eloquent
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Villes';

    public function quartiers()
    {
        return $this->hasMany('Quartier', 'id_ville');
    }

    public static function villeOuEstQuartier($id_ville)

    {
        $i=0;
        $return = array();
        $listeQuartiers = Quartier::where('id_ville', '=', $id_ville)->get();
        foreach ($listeQuartiers as $quartier) {
            $return[$i++] = $quartier->id;
        }
        return $return;
    }
}

?>