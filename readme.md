# Clara De Moliner / Alban Demougin
## Installation
- importer la BDD mySql de test (sources dans sources/script)
- configurer le fichier config.ini (sources/app/config.ini) afin de parametrer l'acces à la BDD
- Mettre à jour composer (dans le terminal = composer update)
- Lancer le site en allant sur _votre-serveur-local/sources/public_

## Utilisation
- La page d'accueil présente toutes les annonces triées dans l'ordre de la plus récente à la plus ancienne.
- En cliquant sur le logo on revient à l'accueil.
- _Ajouter une annonce_ affiche la page d'ajout d'annonce. Les controles sont faits entre le type des informations saisies, et les types attendus en base. Si une information est mal saisie, ou que le mot de passe ne coincide pas entre les 2 champs, un message d'erreur s'affiche. Tous les champs sont obligatoires sauf le prix. Le mot de passe est crypté en Sha1 + sel.
- _retrouver et gerer ses annonces_ permet de chercher ses annonces en saisissant son adresse mail. Si rien n'est trouvé, un message d'erreur s'affiche.
- Le _moteur de recherche_ d'annonces permet de saisir *un ou plusieurs* critères, qui s'additionneront afin d'affiner sa recherche. Les champs *nombre de pieces* et *superficie* peuvent bénéficier d'une marge d'erreur (*+/-*). La encore tous les champs sont controlés et sécurisés avant l'execution de la requete.
- En cliquant sur le titre d'une annonce on arrive à sa description détaillée, et l'on peut la supprimer (en tapant le MDP associé à l'annonce), ou afficher les coordonnées du vendeur.


## Travail réalisé :
- Ce compte rendu
- Le travail de conception dans le dossier _conception_
- Les codes sources, dans le dossier _sources_, envoyées par mail & disponibles sur bitbucket :
[https://bitbucket.org/DMNClara/lebonappart][1]


## Répartition du travail & contribution personnelle :

-\> conception : Clara DM et Alban D

-\> développement :

	-Écriture de la base (Alban D & Clara DM)
	-Relations entre les tables (Clara DM)
	- Configuration slim (Alban D)
	- Routage (Clara DM)
	- Models (Alban D & Clara DM)
	- Twig (Alban D & Clara DM)
	- CSS (Alban D & Clara DM)
	- Afficher une liste d'annonce (Alban D & Clara DM)
	- Afficher le détails d'une annonce (Alban D)
	- Moteur de recherche multi-criteres (Alban D)
	- Controles à l'ajout d'annonce (Alban D)
	- Afficher les coordonnées du vendeur (Clara DM)
	- Gérer ses annonces (Clara DM)
	- Supprimer une annonce (Clara DM)
	- Affichage d'images (Clara DM non terminé)

[1]:	https://bitbucket.org/DMNClara/lebonappart